interface Transforming {
    fun resize(zoom: Double)
    // TODO: величивает фигуру, не перемещая, с сохранением пропорций

    fun rotate(direction: RotateDirection, centerX: Double, centerY: Double)
    // TODO: поворот фигуры вокруг точки (centerX, centerY) на 90 градусов
}

enum class RotateDirection {
    Clockwise, CounterClockwise
}
