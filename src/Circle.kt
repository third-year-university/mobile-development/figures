class Circle(val x: Double, val y: Double, private var r: Double) : Figure() {
    init{
        points = arrayOf(Point(x, y))
    }
    override fun getArea(): Double {
        return Math.PI.toFloat() * r * r
    }

    override fun resize(zoom: Double) {
        r *= zoom
    }

    override val figureName: String
        get() = "Circle"
}