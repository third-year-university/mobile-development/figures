import java.util.Arrays

abstract class Figure : FigureInterface, Transforming, Movable {
    protected lateinit var points: Array<Point>

    abstract fun getArea() : Double

    override fun printPointsCount() {
        if (!this::points.isInitialized) return
        println("Count points of ${figureName}: ${points.size}")
    }

    override fun rotate(direction: RotateDirection, centerX: Double, centerY: Double) {
        for (it in points){
            it.x -= centerX
            it.y -= centerY
            val temp = it.x
            if (direction == RotateDirection.Clockwise){
                it.x = it.y
                it.y = -temp
            }
            else if (direction == RotateDirection.CounterClockwise){
                it.x = -it.y
                it.y = temp
            }
            it.x += centerX
            it.y += centerY
        }

    }


    abstract override fun resize(zoom: Double)

    override fun printPoints() {
        if (!this::points.isInitialized) return
        print("Points of ${figureName}:")
        points.forEach { print(it); print(' ') }
        println()
    }

    override fun move(dx: Double, dy: Double) {
        for (it in points){
            it.x += dx
            it.y += dy
        }
    }
}