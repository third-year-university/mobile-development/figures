import kotlin.math.sqrt

class Point(var x: Double = 0.0, var y: Double = 0.0) {
    fun countDistance(point: Point): Double{
        return sqrt(((this.x - point.x) * (this.x - point.x) + (this.y - point.y) * (this.y - point.y)))
    }

    override fun toString(): String {
        return "($x, $y)"
    }
}