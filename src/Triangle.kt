import kotlin.math.sqrt

class Triangle(x1: Double, y1: Double, x2: Double, y2: Double, x3: Double, y3: Double) : Figure() {

    init {
        points = arrayOf(Point(x1, y1), Point(x2, y2), Point(x3, y3))
    }

    override fun getArea(): Double {
        val p = (side1 + side2 + side3) / 2
        return sqrt(p * (p - side1) * (p - side2) * (p - side3))
    }

    override fun resize(zoom: Double) {
        TODO("Not yet implemented")
    }

    override val figureName: String
        get() = "Triangle"

    val side1: Double
        get() = points[0].countDistance(points[1])
    val side2: Double
        get() = points[1].countDistance(points[2])
    val side3: Double
        get() = points[2].countDistance(points[0])

}