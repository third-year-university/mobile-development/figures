fun main() {
    val rect = Rect(0.0,0.0, 4.0, 5.0)
    rect.printPoints()
    rect.resize(2.0)
    rect.printPoints()
    println()

    rect.move(10.0, 10.0)
    rect.printPoints()
    println()

    rect.rotate(RotateDirection.Clockwise, 0.0, 0.0)
    rect.printPoints()
}