import kotlin.math.abs

class Rect(x: Double, y: Double, width: Double, height: Double) : Figure() {
    init {
        points = arrayOf(Point(x, y), Point(x + width, y + height))
    }
    override fun getArea(): Double {
        return width * height
    }

    override fun resize(zoom: Double) {
        val dX = (width * zoom - width) / 2
        val dY = (height * zoom - height) / 2
        if (points[0].x < points[1].x){
            points[0].x -= dX
            points[1].x += dX
        }
        else{
            points[0].x += dX
            points[1].x -= dX
        }
        if (points[0].y < points[1].y){
            points[0].y -= dY
            points[1].y += dY
        }
        else{
            points[0].y += dY
            points[1].y -= dY
        }
    }

    override val figureName: String
        get() = if (abs(points[0].x - points[1].x) == abs(points[0].y - points[1].y)) "Square" else "Rectangle"

    val width: Double
        get() = abs(points[0].x - points[1].x)

    val height: Double
        get() = abs(points[0].y - points[1].y)

}