interface FigureInterface {
    val figureName: String

    fun printPointsCount()
    fun printPoints()
}
